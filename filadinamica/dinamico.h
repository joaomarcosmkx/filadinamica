
struct no{
  int dado;
  struct no *prox;
};
typedef struct no Lista;



Lista* criaLista(){
  return NULL;
}

Lista * insere(Lista *l, int x){
  Lista * novono = (Lista *) malloc(sizeof(Lista));
  novono->dado = x;
  novono->prox = l;
  return novono;
}

void imprime(Lista *l){
  Lista *aux;
  for(aux=l; aux!=NULL; aux=aux->prox){
    printf("%d",aux->dado);
    printf("\n");
  }
}


Lista* retira(Lista *l, int valor){
  Lista *ant = NULL;
  Lista *aux = l;

   while(aux != NULL && aux->dado!=valor){
     ant = aux;
     aux = aux->prox;
   }
   /*printf("%d\n",ant->dado);
   printf("%d\n", aux->dado);*/
    if(aux == NULL){
      return l;
    }
    if(ant == NULL){
      l = ant->prox;
    }else{
      ant->prox = aux->prox;
    }

      free(aux);
      return l;
}
